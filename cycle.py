import pygame


def main_loop(app, config):
	setup_window(config)
	clock = pygame.time.Clock()
	app.init()

	fps = int(config['DEFAULT']['FPS'])
	dt = 0
	while True:
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				return app.quit()
			app.event(event)
		app.step(dt)
		dt = clock.tick(fps) / 1000
		pygame.display.flip()


def setup_window(config):
	pygame.display.set_mode(
		(
			int(config["RESOLUTION"]["WIDTH"]),
			int(config["RESOLUTION"]["HEIGHT"]),
		),
		eval(config["DEFAULT"]["FLAGS"]),
	)
	pygame.display.set_caption(config["DEFAULT"]["CAPTION"])





