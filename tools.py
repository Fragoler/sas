import sys
import json


def load_json(filename):
	try:
		file = open(filename)
		data = json.load(file)
	except OSError:
		print(f'File `{filename}` not found!')
		sys.exit(1)
	except json.JSONDecodeError as ex:
		file.close()
		print(
			f'Error at `{filename}` '
			+ f'line: {ex.lineno} column: {ex.colno}'
		)
		print(ex.msg)
		sys.exit(1)
	return data


def print_report(filename):
	data = list(load_json(filename))
	total_sounds = sum(map(
		lambda elem: isinstance(elem, dict), data
	))
	cnt = 0
	last_is_sound = False
	for elem in data:
		if not isinstance(elem, dict):
			if last_is_sound:
				print()
			print(elem)
			last_is_sound = False
			continue

		elem = elem.copy()
		print(f'[{cnt + 1}/{total_sounds}]', end='\t')
		print(elem.pop('sound'), end='')
		if elem:
			print('\t\t', elem, sep='')
		else:
			print()
		cnt += 1
		last_is_sound = True




