import pygame

from sound import Sound


class App():

	def __init__(self, sounds, seq):
		self.back_color = 'wheat'
		self.font_color = 'tomato'

		self.sounds = sounds
		self.seq = seq
		self.seq_pos = 0

		self.screen = None
		self.text_prerender = None
		self.font = None

	def init(self):
		self.screen = pygame.display.get_surface()
		self.font = pygame.font.SysFont('mono', 48)

		if not isinstance(self.sounds, dict):
			self.sounds = {}

		sounds = {}
		for sound, file in self.sounds.items():
			sounds[sound] = Sound.load(file)
		self.sounds = sounds

		self.seq = [
			spec for spec in self.seq
			if (
				isinstance(spec, dict)
				and spec['sound'] in self.sounds
			)
		]

		self._next_sound(pos=0)

	def event(self, event):
		if event.type != pygame.KEYDOWN:
			return
		if event.key == pygame.K_RETURN:
			self._restart_sound()
		if event.key == pygame.K_TAB:
			if event.mod & pygame.KMOD_SHIFT:
				self._prev_sound()
			else:
				self._next_sound()
		if event.key == pygame.K_0:
			self._next_sound(0)

	def step(self, dt: float):
		if not self.seq:
			self._update_screen()
			return
		sound = self.sounds[self.seq[self.seq_pos]['sound']]
		if not sound.is_playing() and sound.is_played():
			if self.seq[self.seq_pos].get('autonext'):
				self._next_sound()
		self._update_screen()

	def _update_screen(self):
		self.screen.fill(self.back_color)
		if self.text_prerender is not None:
			self.screen.blit(self.text_prerender, (0, 0))

	def quit(self):
		pass

	def _next_sound(self, pos=None):
		if not self.seq:
			return

		self._stop_sound()
		if pos is None:
			self.seq_pos += 1
		else:
			self.seq_pos = pos
		self.seq_pos %= len(self.seq)
		self._start_sound()

	def _prev_sound(self):
		if not self.seq:
			return

		self._stop_sound()
		self.seq_pos -= 1
		self.seq_pos %= len(self.seq)
		self._start_sound()

	def _start_sound(self):
		if not self.seq:
			return

		spec = self.seq[self.seq_pos].copy()
		sound_title = spec.pop('sound')
		self.sounds[sound_title].start(**spec)

		text = (
			f'[{self.seq_pos + 1}/{len(self.seq)}]'
			+ f' {sound_title}'
		)
		self.text_prerender = self.font.render(
			text, True, self.font_color,
		)

	def _stop_sound(self):
		if not self.seq:
			return
		self.sounds[self.seq[self.seq_pos]['sound']].stop()

	def _restart_sound(self):
		if not self.seq:
			return
		self.sounds[self.seq[self.seq_pos]['sound']].restart()

